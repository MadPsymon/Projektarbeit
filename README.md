# Projektdokumentation zum Projekt

### "Auswahl und Einrichtung einer on Premise gehosteten Cloud-Lösung
**mit LDAP Verbindung zum bestehenden Active Directory, zur Ablösung der bestehenden, extern gehosteten, Cloud"**

## Allgemeines
Repository zur Projektdokumentation von Simon Schäfer zur Abschlussprüfung zum Fachinformatiker Systemintegration Winter 2017/18

## Zeitraum

30.10.2017 - 10.11.2017

## Verwendete Software

* [Visual Studio Code](https://code.visualstudio.com/) (Editor)
  * [German - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-german)
  * [Latex Workshop](https://marketplace.visualstudio.com/items?itemName=James-Yu.latex-workshop)
* [Texlive](https://www.tug.org/texlive/) (Latex Distribution)
* [Mendeley](https://www.mendeley.com/) (Literaturverwaltung)

## Aktuellste Version

[>>> Download <<<](https://gitlab.com/MadPsymon/Projektarbeit/raw/master/main_einseitig.pdf "Aktuellste Version")

## Bewertung
Betriebliche Projektarbeit: **95 Punkte**  
Präsentation/Fachgespräch: **96 Punkte**
